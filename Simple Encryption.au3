#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=SimpleEncryption.ico
#AutoIt3Wrapper_Outfile=Simple Encryption.exe
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Description=Simple Encryption
#AutoIt3Wrapper_Res_Fileversion=1.2.8.0
#AutoIt3Wrapper_Res_LegalCopyright=(C) TheChiefMeat 2016-2018. All rights reserved.
#AutoIt3Wrapper_Res_HiDpi=y
#AutoIt3Wrapper_Res_Field=ProductName|Simple Encryption
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Tidy_Stop_OnError=n
#Au3Stripper_Parameters=/so /rm
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

;==========================================GUI CREATION AND SETTINGS=========================================
#include <GUIConstantsEx.au3>
;!Highly recommended for improved overall performance and responsiveness of the GUI effects etc.! (after compiling):
;Required if you want High DPI scaling enabled. (Also requries _Metro_EnableHighDPIScaling())
#include "MetroGUI-UDF\MetroGUI_UDF.au3"
;Set Theme
_SetTheme("DarkMidnight") ;See MetroThemes.au3 for selectable themes or to add more
;Enable high DPI support: Detects the users DPI settings and resizes GUI and all controls to look perfectly sharp.
_Metro_EnableHighDPIScaling() ; Note: Requries "#AutoIt3Wrapper_Res_HiDpi=y" for compiling. To see visible changes without compiling, you have to disable dpi scaling in compatibility settings of Autoit3.exe
;=============================================================================================================

;INCLUDES - These are required libraries for the script to function.

#include <Crypt.au3>
#include <MsgBoxConstants.au3>
#include <FileConstants.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <WindowsConstants.au3>
#include <Constants.au3>
#include <Array.au3>
#include <File.au3>
#include <StaticConstants.au3>
#include <Encrypt.au3>
#include <Decrypt.au3>
#include <FontConstants.au3>

If FileExists(@AppDataDir & "\TheChiefMeat\Simple Encryption\config.ini") Then
Else
	DirCreate(@AppDataDir & "\TheChiefMeat\Simple Encryption")
	FileOpen(@AppDataDir & "\TheChiefMeat\Simple Encryption\config.ini", 1)
	IniWrite(@AppDataDir & "\TheChiefMeat\Simple Encryption\config.ini", "Startup", "Startup", "0")
	IniWrite(@AppDataDir & "\TheChiefMeat\Simple Encryption\config.ini", "Startup", "Warning", "0")
EndIf

;Here the stored Startup variable is read from the config.ini file that was made before.
$Startup = IniRead(@AppDataDir & "\TheChiefMeat\Simple Encryption\config.ini", "Startup", "Startup", "")

If $Startup = "0" Then
	$LICENCE = _Metro_MsgBox(1, "Simple Encryption 1.2.8", "(C) TheChiefMeat 2016-2018. All rights reserved." & @CRLF & @CRLF & "DISCLAIMER/LICENCE" & @CRLF & @CRLF & "WHILE I HAVE MADE EVERY POSSIBLE EFFORT TO ENSURE THIS SOFTWARE IS BUG FREE," & @CRLF & "I DO NOT GUARANTEE THAT IT IS FREE FROM DEFECTS. THIS SCRIPT IS PROVIDED AS IS" & @CRLF & "AND YOU USE THIS SCRIPT AT YOUR OWN RISK. UNDER NO CIRCUMSTANCE SHALL I BE HELD" & @CRLF & "LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES, LOSSES OR THEFT FROM USE OR MISUSE OF THIS SOFTWARE." & @CRLF & @CRLF & "FOR PRIVATE USE YOU MAY:" & @CRLF & @CRLF & "MODIFY THIS SOFTWARE ANY WAY YOU SEE FIT, HOWEVER SAID MODIFIED VERSIONS ARE NOT TO BE REDISTRIBUTED VIA ANY MEANS." & @CRLF & @CRLF & "FOR PRIVATE USE YOU MAY NOT:" & @CRLF & "REDISTRIBUTE THIS SOFTWARE." & @CRLF & "REDISTRIBUTE MODIFIED VERSIONS OF THIS SOFTWARE." & @CRLF & "COMMERCIALISE THIS SOFTWARE." & @CRLF & "SELL THIS SOFTWARE." & @CRLF & @CRLF & "ANY COMMERCIAL USE OF THIS SOFTWARE IS STRICTLY PROHIBITED." & @CRLF & "ANY COMMERCIALISATION OF THIS SOFTWARE IS STRICTLY PROHIBITED." & @CRLF & @CRLF & "ALL DOWNLOAD LINKS MUST POINT DIRECTLY TO THE BELOW LINK:" & @CRLF & @CRLF & "OFFICIAL BITBUCKET REPOSITORY:" & @CRLF & @CRLF & "https://bitbucket.org/TheChiefMeat/simple-encryption/downloads/simple-encryption-latest.zip", 650, 11)
	If $LICENCE = "Cancel" Then
		FileDelete(@AppDataDir & "\TheChiefMeat\Simple Encryption\config.ini")
		Exit 0
	ElseIf $LICENCE = "OK" Then
		IniWrite(@AppDataDir & "\TheChiefMeat\Simple Encryption\config.ini", "Startup", "Startup", "1")
		FileInstall("PATCH NOTES.TXT", @AppDataDir & "\TheChiefMeat\Simple Encryption\PATCH NOTES.TXT")
	EndIf
Else
EndIf

$Form1 = _Metro_CreateGUI("Simple Encryption", 166, 390, -1, -1, True)
$Encrypt = _Metro_CreateButton("Encrypt", 18, 20, 130, 40)
$Decrypt = _Metro_CreateButton("Decrypt", 18, 65, 130, 40)
$Guide = _Metro_CreateButton("Guide", 18, 110, 130, 40)
$EXIT = _Metro_CreateButton("Exit", 18, 155, 130, 40)
$ButtonBKColor = "0x40C338"
$Donate = _Metro_CreateButton("Donate", 18, 200, 130, 40)
$ButtonBKColor = "0x3C4D66"
$Password = GUICtrlCreateInput("", 18, 253, 130, 20)
GUICtrlSetFont($Password, 10)
$Toggle1 = _Metro_CreateToggle("Source Delete", 23, 330, 130, 40)
$Toggle2 = _Metro_CreateToggle("PC/Keyfile", 23, 280, 130, 40)

GUICtrlSetResizing($Encrypt, $GUI_DOCKALL + $GUI_DOCKBORDERS)
GUICtrlSetResizing($Decrypt, $GUI_DOCKALL + $GUI_DOCKBORDERS)
GUICtrlSetResizing($EXIT, $GUI_DOCKALL + $GUI_DOCKBORDERS)
GUICtrlSetResizing($Password, $GUI_DOCKALL + $GUI_DOCKBORDERS)
GUICtrlSetResizing($Toggle1, $GUI_DOCKALL + $GUI_DOCKBORDERS)
GUICtrlSetResizing($Toggle2, $GUI_DOCKALL + $GUI_DOCKBORDERS)
GUICtrlSetResizing($Guide, $GUI_DOCKALL + $GUI_DOCKBORDERS)
GUICtrlSetResizing($Donate, $GUI_DOCKALL + $GUI_DOCKBORDERS)

GUISetState(@SW_SHOW)
While 1
	_Metro_HoverCheck_Loop($Form1)
	Switch GUIGetMsg()
		Case $Toggle1
			If _Metro_ToggleIsChecked($Toggle1) Then
				_Metro_ToggleUnCheck($Toggle1)
			Else
				_Metro_ToggleCheck($Toggle1)
			EndIf
		Case $Toggle2
			If _Metro_ToggleIsChecked($Toggle2) Then
				_Metro_ToggleUnCheck($Toggle2)
			Else
				_Metro_ToggleCheck($Toggle2)
			EndIf
		Case $Encrypt
			Encrypt()
		Case $Decrypt
			Decrypt()
		Case $Guide
			$GuideMSG = _Metro_MsgBox(0, "Simple Encryption 1.2.8", "To encrypt a file, simply supply a password and then select the file you wish to encrypt." & @CRLF & @CRLF & "If you don't want to use a password, use the PC/Keyfile toggle switch. The PC setting allows only the current PC to decrypt your file, whereas the keyfile setting allows only the generated keyfile to decrypt your files." & @CRLF & @CRLF & "Using the Source Delete toggle switch allows you to delete the source file, use this switch with caution!", 280, 11)
		Case $Donate
			ShellExecute("https://www.paypal.me/TheChiefMeat/3")
		Case $EXIT
			FileDelete('.enc')
			$EXIT
			Exit 0
			ExitLoop
	EndSwitch
WEnd
GUIDelete($Form1)
