(C) TheChiefMeat 2016-2018. All rights reserved.

Simple Encryption is an easy to use encryption program for Windows.

Installation:

Extract the .zip file to a location of your choice.
Run the Simple Encryption.exe

Features:

Single and multi-file encryption.
Encryption with Password support.
Encryption with PC/Hard Drive.

Download link:

https://bitbucket.org/TheChiefMeat/simple-encryption/downloads/simple-encryption-latest.zip

To donate please go to https://www.paypal.me/TheChiefMeat/3